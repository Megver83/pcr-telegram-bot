#!/usr/bin/env python3
from sh import sed, libreoffice
from random import randint
from os import remove
import datetime

def create_pcr(
        fullname: str,
        run: str,
        bday: datetime,
        pcr_template: str,
        in_datetime = None,
        output_dir = "/tmp"
    ):
    # Formato de Integramédica
    fullname = fullname.upper()
    run = run.upper()
    ftime = lambda dtime: dtime.strftime("%d/%m/%Y %H:%M")

    # Calcula la edad
    # https://stackoverflow.com/questions/2217488/age-from-birthdate-in-python
    today = datetime.date.today()
    age = today.year - bday.year - ((today.month, today.day) < (bday.month, bday.day))

    # Fecha de nacimiento
    birthday = bday.strftime("%d/%m/%Y")

    # Genera un número de solicitud aleatorio
    request = randint(30000000, 40000000)

    if not in_datetime:
        now = datetime.datetime.now()
        hour = randint(10, 11)
        minutes = randint(0, 59)
        in_datetime = datetime.datetime(now.year, now.month, now.day, hour, minutes)
        in_datetime -= datetime.timedelta(days = 1)

    # Deltas aleatorios.
    # La mayor diferencia que puede haber entre la hora de ingreso y de impresión es de 3,5 hrs.
    # Hora de extracción. Unos 2 a 10 minutos después del ingreso.
    ext_datetime = in_datetime + datetime.timedelta(minutes = randint(2, 10))
    # Fecha de recepción.
    rec_datetime = ext_datetime + datetime.timedelta(hours = 1, minutes = randint(0, 30))
    # Fecha de validación. Entre 1 hr y 1,5 hrs desde la fecha de recepción.
    validation_datetime = rec_datetime + datetime.timedelta(hours = 1, minutes = randint(0, 30))
    # Fecha de impresión. Entre 5 a 20 minutos después de la validación.
    print_datetime = validation_datetime + datetime.timedelta(minutes = randint(5, 20))

    output_filename = f"PCR_IntegraMedica_{request}_" + in_datetime.strftime("%d-%m-%Y")
    output_fodt = output_dir + "/" + output_filename + ".fodt"
    output_pdf = output_dir + "/" + output_filename + ".pdf"

    with open(output_fodt, 'w', encoding = 'utf-8') as output_file:
        sed(
            f"s|%FULL_NAME%|{fullname}|g;"
            f"s|%AGE%|{age}|g;"
            f"s|%RUT%|{run}|g;"
            f"s|%BIRTHDAY%|{birthday}|g;"
            f"s|%REQUEST%|{request}|g;"
            f"s|%ING_DATETIME%|{ftime(in_datetime)}|g;"
            f"s|%EXT_DATETIME%|{ftime(ext_datetime)}|g;"
            f"s|%REC_DATETIME%|{ftime(rec_datetime)}|g;"
            f"s|%VALIDATION_DATETIME%|{ftime(validation_datetime)}|g;"
            f"s|%PRINT_DATETIME%|{ftime(print_datetime)}|g",
            pcr_template, _out=output_file
        )

    libreoffice("--convert-to", "pdf", "--outdir", output_dir, output_fodt)
    remove(output_fodt)

    return open(output_pdf, "rb")
