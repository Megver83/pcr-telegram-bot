#!/usr/bin/env python3
'''
Bot de Telegram para crear y enviar PCRs.

Como funciona (aparte de lo visiblemente obvio)
-----------------------------------------------
    set_profile y birth_calendar retornan funciones guardadas en el diccionario chat_data a los que
    se les llama "return_hook". Estos hooks reciben como parámetros el objeto CallbackContext y un
    int que es el chat_id.

    Los hooks son:
        * ask_name (No se guarda en chat_id, se usa directamente cuando es necesario)
        * ask_pcr_date_getter
        * create_profile
        * show_profile
'''
import logging

from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    Update,
    ReplyKeyboardRemove,
    ReplyKeyboardMarkup,
    ParseMode
)
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    CallbackQueryHandler,
    ConversationHandler,
    PicklePersistence,
    CallbackContext,
)
from emoji import emojize
from pcr import create_pcr
import utils
import datetime
import regex
import os
import json
import sys

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

CREATE, BDAY, ATTENTION, IDENTITY, USE_PROFILE, MANAGE_PROFILE, EDIT_PROFILE = range(7)

try:
    with open("config.json", "r") as config_json:
        config_data = json.load(config_json)
        token = config_data['token']
except FileNotFoundError:
    sys.exit("El archivo 'config.json' no fue encontrado. Utilice 'config.example.json' como plantilla")


def start(update, context):
    update.message.reply_text("Crea fácilmente tu propio PCR con /pcr. Escribe /ayuda para aprender más.")


def help_handler(update, context):
    update.message.reply_text(
        "*Comandos*\n" +
        "/pcr \- Crea un PCR en formato PDF\n" +
        "/nuevoperfil \- Guarda nombres, RUNs y fechas de nacimiento\n" +
        "/perfiles \- Gestiona tus identidades para usar en los PCRs\n" +
        "/cancelar \- Cancela la operación en curso, si hay alguna\n" +
        "/ayuda \- Muestra esta ayuda\n" +
        "/info \- Explica el funcionamiento del bot y da recomendaciones",
        parse_mode = ParseMode.MARKDOWN_V2
    )


def info_handler(update, context):
    update.message.reply_text(
        "*Acerca de la fecha de ingreso*\n" +
        "Cuando creas un PCR con /pcr, puedes escoger la fecha de ingreso o " +
        "dejar que el bot use automáticamente una de menos de 48 hrs atrás\. " +
        "Esta fecha automática es un día atrás entre las 10:00 y 11:59 hrs\.\n\n" +
        "*Diferencias de tiempo entre las fechas*\n" +
        "Las fechas listadas aquí se generan en base al ingreso y no se pueden escoger:\n" +
        "_*Fecha de extracción*_ \- 2 a 10 minutos después del ingreso\n" +
        "_*Fecha de recepción*_ \- 1 hora a 1 hora y media después de la extracción\n" +
        "_*Fecha de validación*_ \- 1 hora a 1 hora y media después de la recepción\n" +
        "_*Fecha de obtención*_ \- 5 a 20 minutos después de la validación\n\n" +
        "*Recomendaciones*\n" +
        "Ten en cuenta las fechas arriba mencionadas y los [horarios de atención de tu " +
        "clínica IntegraMédica](https://www.integramedica.cl/integramedica/site/edic/base/port/sucursales_horarios.html) " +
        "por la que harás pasar tu PCR\. Por lo general, convendrá escoger la fecha " +
        "de ingreso manualmente para los días Sábado y Domingo, en lugar auto generarla\.\n" +
        "Recuerda que puedes editar los PDFs utilizando software como [LibreOffice](https://www.libreoffice.org/) Draw",
        parse_mode = ParseMode.MARKDOWN_V2,
        disable_web_page_preview = True
    )


def ask_userdata_source(update, context):
    update.message.reply_text(
        text = "¿Cómo desea obtener el nombre, RUN y fecha de nacimiento?\n" +
               "*Perfil*: Selecciona un perfil y ahórrate escribir esos datos más adelante\n" +
               "*Manual*: Ingresa manualmente la información a continuación",
        parse_mode = ParseMode.MARKDOWN_V2,
        reply_markup = InlineKeyboardMarkup.from_row([
            InlineKeyboardButton("Perfil", callback_data = "USE_PROFILE"),
            InlineKeyboardButton("Manual", callback_data = "NO_PROFILE")
        ])
    )
    return CREATE


def new_profile(update, context):
    # Después de tener la fecha de nacimiento, crear el perfil
    context.chat_data['birth_calendar_return_hook'] = create_profile
    return ask_name(context, update.effective_message.chat_id)


def create_from_input(update, context):
    # Borra el mensaje de ask_userdata_source
    update.callback_query.message.delete()

    # Después de tener la fecha de nacimiento, preguntar de donde sacar la fecha del PCR
    context.chat_data['birth_calendar_return_hook'] = ask_pcr_date_getter
    return ask_name(context, update.callback_query.message.chat_id)


def ask_name(context, chat_id):
    context.bot.send_message(
        text = "Ingrese su nombre completo en el siguiente mensaje",
        chat_id = chat_id
    )

    # Inicializa algunos datos
    context.chat_data['fullname'] = None
    context.chat_data['RUN'] = None
    return IDENTITY


def validate_run(run: str):
    return bool(regex.fullmatch("[1-9]\d?[.]\d{3,3}[.]\d{3,3}-(K|k|\d)", run))


def validate_name(name: str):
    # Usa el comportamiento V1 del módulo regex
    return bool(regex.fullmatch("(?V1)^[\s\p{L}--ªº]+", name))


def set_identity(update, context):
    """Guarda el nombre completo y RUN en chat_data"""
    text = update.message.text

    if context.chat_data['fullname']:
        if validate_run(text):
            context.chat_data['RUN'] = text
        else:
            update.message.reply_text("RUN inválido, por favor, vuelva a escribirlo.")
            return
    else:
        if validate_name(text):
            context.chat_data['fullname'] = text
            update.message.reply_text("Ingrese su RUN con puntos y guión")
        else:
            update.message.reply_text("Nombre inválido, sólo se permiten caracteres alfabéticos, intente de nuevo")
        return

    # Siempre se pregunta por la fecha de nacimiento después del nombre y RUN
    return ask_birth_year(update, context)


def ask_birth_year(update, context):
    r, c = 8, 4
    update.message.reply_text(
        text = "Escoge el año de nacimiento",
        reply_markup = utils.create_time_inkey(
            "BDAY", "YEAR",
            start = datetime.datetime.now().year + 1 - r * c,
            rows = r, cols = c,
            nav_keys = True,
            lim_down = 1900, lim_up = datetime.datetime.now().year
        )
    )
    return BDAY


def select_birth_date(update, context):
    query = update.callback_query
    action, data = utils.separate_callback_data(query.data)
    yearmonth = int(data)

    match action:
        case "YEAR":
            # Año seleccionado. Pregunta por el mes.
            context.chat_data['byear'] = yearmonth
            query.message.delete()
            context.bot.send_message(
                text = "Seleccione el mes de nacimiento:",
                chat_id = query.message.chat_id,
                reply_markup = utils.create_month_inkey("BDAY")
            )
        case "MOVE-YEAR":
            # Muestra otro rango de años.
            context.bot.edit_message_text(
                text = query.message.text,
                chat_id=query.message.chat_id,
                message_id=query.message.message_id,
                reply_markup = utils.create_time_inkey(
                    "BDAY", "YEAR",
                    start = yearmonth,
                    rows = 8, cols = 4,
                    nav_keys = True,
                    lim_down = 1900, lim_up = datetime.datetime.now().year
                )
            )
        case "MONTH":
            # Mes seleccionado. Pregunta por el día y crea un calendario
            context.chat_data['bmonth'] = yearmonth
            query.message.delete()
            context.bot.send_message(
                text = "Seleccione el día de nacimiento",
                chat_id=query.message.chat_id,
                reply_markup=utils.create_calendar(context.chat_data['byear'], context.chat_data['bmonth'])
            )

    return BDAY


def birth_calendar(update, context):
    """Guarda la fecha de nacimiento en chat_data"""
    selected, date = utils.process_calendar_selection(update, context)
    if selected:
        context.chat_data['birth'] = date
        return context.chat_data['birth_calendar_return_hook'](context, update.callback_query.from_user.id)


def create_profile(context, chat_id):
    # Guarda el nombre con las puras iniciales en mayúscula
    fullname = context.chat_data['fullname'].title()
    RUN = context.chat_data['RUN']
    birth = context.chat_data['birth']

    context.user_data[fullname] = {'RUN': RUN, 'birth': birth}
    context.bot.send_message(
        text = "Perfil creado exitosamente",
        chat_id = chat_id
    )
    return ConversationHandler.END


def profile_options(update, context):
    if select_profile(context, update.effective_chat.id):
        context.chat_data['set_profile_return_hook'] = show_profile
        return USE_PROFILE
    return ConversationHandler.END


def create_from_profile(update, context):
    # Borra el mensaje de ask_userdata_source
    update.callback_query.message.delete()

    if select_profile(context, update.callback_query.message.chat_id):
        context.chat_data['set_profile_return_hook'] = ask_pcr_date_getter
        return USE_PROFILE
    return ConversationHandler.END


def select_profile(context, chat_id):
    """Retorna True si hay algún perfil, False de lo contrario"""
    if len(context.user_data) == 0:
        context.bot.send_message(
            text = "No hay perfiles, puede crear uno con /nuevoperfil",
            chat_id = chat_id
        )
        return False

    context.bot.send_message(
        text = "Selecciona un perfil",
        chat_id = chat_id,
        reply_markup = ReplyKeyboardMarkup.from_column(
            list(context.user_data),
            resize_keyboard = True
        )
    )
    return True


def set_profile(update, context):
    fullname = update.message.text
    update.message.reply_text(
        text = "Perfil seleccionado",
        reply_markup = ReplyKeyboardRemove()
    )

    if fullname not in context.user_data:
        update.message.reply_text(
            "El perfil especificado no existe, puede crearlo con /nuevoperfil o escoger otro",
            reply_markup = ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    context.chat_data['fullname'] = fullname
    context.chat_data['RUN'] = context.user_data[fullname]['RUN']
    context.chat_data['birth'] = context.user_data[fullname]['birth']

    return context.chat_data['set_profile_return_hook'](context, update.effective_message.chat_id)


def show_profile(context, chat_id):
    fullname = context.chat_data['fullname']
    RUN = context.chat_data['RUN'].replace('.', '\.').replace('-', '\-')
    birth = context.chat_data['birth']

    context.bot.send_message(
        chat_id = chat_id,
        text = f"*Nombre*: {fullname}\n" +
               f"*RUN*: {RUN}\n" +
                "*Fecha de nacimiento*: " + birth.strftime("%d/%m/%Y"),
        parse_mode = ParseMode.MARKDOWN_V2,
        reply_markup = InlineKeyboardMarkup.from_row([
            InlineKeyboardButton(emojize(":x: Eliminar", language = 'alias'), callback_data = "DELETE"),
            InlineKeyboardButton(emojize(":pencil: Editar", language = 'alias'), callback_data = "EDIT"),
            InlineKeyboardButton(emojize(":heavy_check_mark: Listo", language = 'alias'), callback_data = "DONE")
        ])
    )
    return MANAGE_PROFILE


def delete_profile(update, context):
    # Borra el perfil de user_data
    context.user_data.pop(context.chat_data['fullname'])
    update.callback_query.message.delete()

    context.bot.send_message(
        text = "Perfil eliminado exitosamente",
        chat_id = update.callback_query.message.chat_id
    )

    return ConversationHandler.END


def edit_profile(update, context):
    context.chat_data['editing'] = None
    update.callback_query.message.delete()

    context.bot.send_message(
        text = "Escoge el campo que deseas modificar",
        chat_id = update.callback_query.message.chat_id,
        reply_markup = ReplyKeyboardMarkup(
            [
                ['Nombre', 'RUN'],
                ['Fecha de nacimiento']
            ],
            resize_keyboard = True
        )
    )
    return EDIT_PROFILE


def choose_edit(update, context):
    if not context.chat_data['editing']:
        context.chat_data['editing'] = update.message.text

        match context.chat_data['editing']:
            case 'Nombre':
                update.message.reply_text(
                    text = "Ingresa el nuevo nombre",
                    reply_markup = ReplyKeyboardRemove()
                )
            case 'RUN':
                update.message.reply_text(
                    text = "Ingresa el nuevo RUN, con puntos y guión",
                    reply_markup = ReplyKeyboardRemove()
                )
            case 'Fecha de nacimiento':
                update.message.reply_text(
                    text = "Se va a cambiar la fecha de nacimiento",
                    reply_markup = ReplyKeyboardRemove()
                )
                context.chat_data['birth_calendar_return_hook'] = update_birth
                return ask_birth_year(update, context)


def process_profile_editing(update, context):
    text = update.message.text
    editing = context.chat_data['editing']

    if editing:
        fullname = context.chat_data['fullname']
        match editing:
            case 'Nombre':
                text = text.title()
                if validate_name(text):
                    # Hace una nueva palabra clave con los mismos datos que antes,
                    # ya que las palabras clave en user_data son los nombres
                    context.user_data[text] = context.user_data[fullname]
                    # Actualiza el dato no persistente
                    context.chat_data['fullname'] = text
                    # Elimina el antiguo nombre
                    context.user_data.pop(fullname)

                    update.message.reply_text(
                        "Perfil renombrado\n" +
                        f"Antes era: *{fullname}*\n" +
                        f"Ahora es: *{text}*",
                        parse_mode = ParseMode.MARKDOWN_V2
                    )

                    # Vuelve a show_profile
                    return show_profile(context, update.effective_message.chat_id)
                update.message.reply_text("Nombre inválido, sólo se permiten caracteres alfabéticos, intente de nuevo")
            case 'RUN':
                if validate_run(text):
                    context.user_data[fullname]['RUN'] = text
                    context.chat_data['RUN'] = text

                    update.message.reply_text("RUN actualizado a " + text)
                    return show_profile(context, update.effective_message.chat_id)
                update.message.reply_text("RUN inválido, por favor, vuelva a escribirlo.")
    else:
        update.message.reply_text(text = "Campo a editar inválido, intente con otro")


def update_birth(context, chat_id):
    fullname = context.chat_data['fullname']
    date = context.chat_data['birth']
    context.user_data[fullname]['birth'] = date

    context.bot.send_message(
        text = "Fecha de nacimiento actualizada a " + date.strftime("%d/%m/%Y"),
        chat_id = chat_id
    )
    return show_profile(context, chat_id)


def finish_profile_job(update, context):
    update.callback_query.message.delete()
    return ConversationHandler.END


def ask_pcr_date_getter(context, chat_id):
    context.bot.send_message(
        chat_id = chat_id,
        text = "¿Elegir horario de atención?\n" +
               "*Si*: Se le preguntará por el día y hora que aparecerá en el PCR\n" +
               "*No*: Genera una fecha automáticamente de no más de 48 horas atrás",
        parse_mode = ParseMode.MARKDOWN_V2,
        reply_markup = InlineKeyboardMarkup.from_row([
            InlineKeyboardButton("Si", callback_data = "AUTO;YES"),
            InlineKeyboardButton("No", callback_data = "AUTO;NO")
        ])
    )
    return ATTENTION


def attention_auto_handler(update, context):
    query = update.callback_query
    value = utils.separate_callback_data(query.data)
    context.chat_data['fastpcr'] = False

    match value[0]:
        case "YES":
            context.bot.edit_message_text(
                text = "Escoge el día de la atención",
                chat_id = query.message.chat_id,
                message_id = query.message.message_id,
                reply_markup = utils.create_calendar()
            )
        case "NO":
            # Crear un PCR que calcule una fecha válida
            return make_pcr(update, context, fast = True)


def attention_calendar_handler(update, context):
    selected, date = utils.process_calendar_selection(update, context)
    if selected:
        # Inicializa el día seleccionado a las 8:00 AM
        context.chat_data['attention'] = datetime.datetime(date.year, date.month, date.day, 8, 0)
        context.bot.send_message(
            chat_id = update.callback_query.from_user.id,
            text = "Escoge una hora\n" +
                   "Hora seleccionada: " + context.chat_data['attention'].strftime("*%H:%M*"),
            parse_mode = ParseMode.MARKDOWN_V2,
            reply_markup = utils.create_hour_inkey()
        )


def attention_hour_handler(update, context):
    query = update.callback_query
    action, value = utils.separate_callback_data(query.data)

    match action:
        case "HOUR":
            context.chat_data['attention'] += datetime.timedelta(hours = int(value))
        case "MIN":
            context.chat_data['attention'] += datetime.timedelta(minutes = int(value))

    context.bot.edit_message_text(
        text = "Escoge una hora\n" +
               "Hora seleccionada: " + context.chat_data['attention'].strftime("*%H:%M*"),
        parse_mode = ParseMode.MARKDOWN_V2,
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        reply_markup = utils.create_hour_inkey()
    )


def make_pcr(update, context, fast = False):
    """Crea el archivo PCR, lo envía y borra la copia local"""
    user = update.callback_query.from_user
    update.callback_query.message.delete()

    if fast:
        pcr_pdf = create_pcr(
            context.chat_data['fullname'],
            context.chat_data['RUN'],
            context.chat_data['birth'],
            pcr_template = "PCR.fodt"
        )
    else:
        pcr_pdf = create_pcr(
            context.chat_data['fullname'],
            context.chat_data['RUN'],
            context.chat_data['birth'],
            pcr_template = "PCR.fodt",
            in_datetime = context.chat_data['attention']
        )

    # Ya que siempre es llamado desde funciones que reciben un callback_query,
    # puedo obtener el chat_id de las misma forma
    logger.info("Sending %s to user %s.", pcr_pdf.name, user.first_name)
    context.bot.send_document(
        document = pcr_pdf,
        chat_id = update.callback_query.message.chat_id
    )
    os.remove(pcr_pdf.name)

    return ConversationHandler.END


def cancel(update: Update, context: CallbackContext) -> int:
    """Cancela y termina la conversación."""
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        'Operación cancelada.',
        reply_markup = ReplyKeyboardRemove()
    )
    return ConversationHandler.END


def main() -> None:
    """Ejecuta el bot."""
    # Crea un archivo de persistencia solo para user_data
    persistence = PicklePersistence(
        filename = 'pcrbot',
        store_chat_data = False,
        store_bot_data = False
    )
    # Crea el Updater con persistencia y le pasa el token del bot.
    updater = Updater(token, persistence = persistence)

    # Obtiene el dispatcher para registrar handlers
    dispatcher = updater.dispatcher

    # Utiliza el parámetro pattern para pasar CallbackQueries con patrones de datos
    # específicos a los controladores correspondientes.
    conv_handler = ConversationHandler(
        entry_points = [
            CommandHandler('pcr', ask_userdata_source),
            CommandHandler('perfiles', profile_options),
            CommandHandler('nuevoperfil', new_profile)
        ],
        states = {
            CREATE: [
                CallbackQueryHandler(create_from_input, pattern = "^NO_PROFILE$"),
                CallbackQueryHandler(create_from_profile, pattern = "^USE_PROFILE$"),
            ],
            BDAY: [
                CallbackQueryHandler(select_birth_date, pattern = "^BDAY;(YEAR|MOVE-YEAR|MONTH)"),
                CallbackQueryHandler(birth_calendar, pattern = '^CALENDAR;')
            ],
            ATTENTION: [
                CallbackQueryHandler(attention_auto_handler, pattern = "^AUTO;(YES|NO)$"),
                CallbackQueryHandler(attention_calendar_handler, pattern = '^CALENDAR;'),
                CallbackQueryHandler(attention_hour_handler, pattern = "^TIME;(HOUR|MIN);"),
                CallbackQueryHandler(make_pcr, pattern = "^TIME;DONE$")
            ],
            IDENTITY: [
                MessageHandler(Filters.text & ~(Filters.command), set_identity)
            ],
            USE_PROFILE: [
                MessageHandler(Filters.text & ~(Filters.command), set_profile)
            ],
            MANAGE_PROFILE: [
                CallbackQueryHandler(delete_profile, pattern = "^DELETE$"),
                CallbackQueryHandler(edit_profile, pattern = "^EDIT$"),
                CallbackQueryHandler(finish_profile_job, pattern = "^DONE$"),
            ],
            EDIT_PROFILE: [
                MessageHandler(Filters.regex("^(Nombre|RUN|Fecha de nacimiento)$"), choose_edit),
                MessageHandler(Filters.text & ~(Filters.command), process_profile_editing),
            ]
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
        name = 'pcrbot',
        persistent = True
    )

    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('ayuda', help_handler))
    dispatcher.add_handler(CommandHandler('info', info_handler))
    # Agrega ConversationHandler al dispatcher que usaremos para manejar actualizaciones
    dispatcher.add_handler(conv_handler)

    # Inicia el bot
    updater.start_polling()

    # Ejecuta el bot hasta que se presione Ctrl-C o el proceso reciba SIGINT,
    # SIGTERM o SIGABRT. Esto debería usarse la mayor parte del tiempo, ya que
    # start_polling() no bloquea y detendrá el bot con gracia.
    updater.idle()


if __name__ == '__main__':
    main()
