#!/usr/bin/env python3
'''
Basado en https://github.com/unmonoqueteclea/calendar-telegram

Formato callback_data:
    TIPO;ACCION;DATO1;DATO2;DATO3;...
TIPO y ACCION no pueden estar vacíos

Guarda los datos en el dict context.chat_data:
    ['attention'] = datetime.datetime(attention)
    ['birth'] = datetime.date(birthday)  // Con estos datos creo el PCR, 'byear' y 'bmonth' son para iniciar el calendario
    ['byear'] = int(year)
    ['bmonth'] = int(month)

Tipos de callbacks. En minúsculas las variables. Ordenados según como debieran ser llamados.
Callbacks de calendario:
    CALENDAR;DAY;year;month;day     // Se selecciona un día
    CALENDAR;PREV-MONTH;year;month  // Mes anterior, muestra otro InlineKeyboardMarkup
    CALENDAR;NEXT-MONTH;year;month  // Lo mismo^ pero con el mes siguiente
    CALENDAR;IGNORE                 // Para los botones vacíos, no hace nada
Callbacks de fecha de nacimiento:
    BDAY;YEAR;year
    BDAY;MOVE-YEAR;year             // create_time_inkey() dejará 'year' como el primer año del rango a mostrar
    BDAY;MONTH;month
    // Calendario
Callbacks de hora de atención:
    // Calendario
    ATTENTION:AUTO;bool (bool in ("NO", "YES"))
    ATTENTION;HOUR;hour
    ATTENTION;MIN;minute
    ATTENTION:DONE

Todas las variables son números enteros, a no ser que se especifique lo contrario

En el futuro:
    - Preguntar por confirmación, mostrando los datos antes de crear el PCR
'''

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from emoji import emojize
import datetime
import calendar
import locale

# Para que los nombres de los meses estén en español.
# En caso de error, revisar los locales disponibles con `locale -a`
locale.setlocale(locale.LC_ALL, 'spanish')

def create_callback_data(*data):
    return ";".join([str(d) for d in data])

def separate_callback_data(cdata, start = 1):
    return cdata.split(";")[start:]

def create_calendar(year = None, month = None):
    """
    Create an inline keyboard with the provided year and month
    :param int year: Year to use in the calendar, if None the current year is used.
    :param int month: Month to use in the calendar, if None the current month is used.
    :return: Returns the InlineKeyboardMarkup object with the calendar.
    """
    today = datetime.date.today()
    if not year: year = today.year
    if not month: month = today.month

    data_ignore = create_callback_data("CALENDAR", "IGNORE")
    keyboard = []

    #First row - Month and Year
    row=[]
    row.append(InlineKeyboardButton(calendar.month_name[month].capitalize() + " " + str(year), callback_data = data_ignore))
    keyboard.append(row)

    #Second row - Week Days
    row=[]
    for day in "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do":
        row.append(InlineKeyboardButton(day, callback_data = data_ignore))
    keyboard.append(row)

    my_calendar = calendar.monthcalendar(year, month)
    for week in my_calendar:
        row=[]
        for day in week:
            if(day==0):
                row.append(InlineKeyboardButton(" ",callback_data = data_ignore))
            else:
                row.append(InlineKeyboardButton(str(day), callback_data = create_callback_data("CALENDAR", "DAY", year, month, day)))
        keyboard.append(row)

    #Last row - Buttons
    row=[]
    row.append(InlineKeyboardButton(emojize(":arrow_backward:", language='alias'), callback_data = create_callback_data("CALENDAR", "PREV-MONTH", year, month)))
    row.append(InlineKeyboardButton(emojize(":arrow_forward:", language='alias'), callback_data = create_callback_data("CALENDAR", "NEXT-MONTH", year, month)))
    keyboard.append(row)

    return InlineKeyboardMarkup(keyboard)


def create_month_inkey(kind):
    months = [m.capitalize() for m in calendar.month_abbr][1:]
    cols = 4
    index = 0
    keyboard = []

    while index <= 12:
        row = []
        for m in months[index:index+cols]:
            month_number = list(calendar.month_abbr).index(m.lower())  # lower() es necesario en locale español
            row.append(InlineKeyboardButton(m, callback_data = create_callback_data(kind, "MONTH", str(month_number))))
        keyboard.append(row)
        index += cols

    return InlineKeyboardMarkup(keyboard)


def create_time_inkey(kind, action, start, rows, cols, nav_keys = False, lim_down = 0, lim_up = None):
    # lim_up and lim_down are included
    keyboard = []
    factor = 0
    buttons = rows * cols
    end = start + buttons
    range_num = range(start, end)

    while factor <= rows:
        row = []

        for time in range_num[factor * cols : factor * cols + cols]:
            if time < lim_down: continue
            if lim_up and time > lim_up: continue

            time_str = str(time)
            if time < 10: time_str = "0" + time_str
            row.append(InlineKeyboardButton(time_str, callback_data = create_callback_data(kind, action, time_str)))

        keyboard.append(row)
        factor += 1

    if nav_keys:
        row = []
        if start > lim_down:
            row.append(InlineKeyboardButton(emojize(":arrow_backward:", language='alias'), callback_data = create_callback_data(kind, "MOVE-" + action, range_num[0] - buttons)))

        if not lim_up or end < lim_up:
            row.append(InlineKeyboardButton(emojize(":arrow_forward:", language='alias'), callback_data = create_callback_data(kind, "MOVE-" + action, range_num[0] + buttons)))
        keyboard.append(row)

    return InlineKeyboardMarkup(keyboard)


def create_hour_inkey(kind = "TIME"):
    keyboard = [
        # First row
        [
            InlineKeyboardButton(emojize(":minus: 1 hr", language = 'alias'), callback_data = create_callback_data(kind, "HOUR", "-1")),
            InlineKeyboardButton(emojize(":plus: 1 hr", language = 'alias'), callback_data = create_callback_data(kind, "HOUR", "1"))
        ],
        # Second row
        [
            InlineKeyboardButton(emojize(":minus: 1 min", language = 'alias'), callback_data = create_callback_data(kind, "MIN", "-1")),
            InlineKeyboardButton(emojize(":plus: 1 min", language = 'alias'), callback_data = create_callback_data(kind, "MIN", "1"))
        ],
        # Third row
        [
            InlineKeyboardButton(emojize(":minus: 5 min", language = 'alias'), callback_data = create_callback_data(kind, "MIN", "-5")),
            InlineKeyboardButton(emojize(":plus: 5 min", language = 'alias'), callback_data = create_callback_data(kind, "MIN", "5"))
        ],
        # Fourth row
        [
            InlineKeyboardButton(emojize(":minus: 10 min", language = 'alias'), callback_data = create_callback_data(kind, "MIN", "-10")),
            InlineKeyboardButton(emojize(":plus: 10 min", language = 'alias'), callback_data = create_callback_data(kind, "MIN", "10"))
        ],
        # Fifth row
        [InlineKeyboardButton("Hecho", callback_data = create_callback_data(kind, "DONE"))]
    ]
    return InlineKeyboardMarkup(keyboard)


def process_calendar_selection(update, context) -> tuple:
    """
    Process the callback_query. This method generates a new calendar if forward or
    backward is pressed. This method should be called inside a CallbackQueryHandler.
    :param telegram.Bot bot: The bot, as provided by the CallbackQueryHandler
    :param telegram.Update update: The update, as provided by the CallbackQueryHandler
    :return: Returns a tuple (Boolean,datetime.datetime,str), indicating if a date is selected
                and returning the date if so.
    """
    ret_data = (False, None)
    query = update.callback_query
    #print(query)

    #DAY[0];year[1];month[2];day[3]
    data = separate_callback_data(query.data)
    action = data[0]
    if action in ("DAY", "PREV-MONTH", "NEXT-MONTH"):
        year, month = int(data[1]), int(data[2])
        curr = datetime.datetime(year, month, 1)

    re_create_calendar = lambda new: context.bot.edit_message_text(
        text=query.message.text,
        chat_id=query.message.chat_id,
        message_id=query.message.message_id,
        reply_markup=create_calendar(new.year, new.month)
    )

    match action:
        case "IGNORE":
            pass
        case "DAY":
            day = int(data[3])
            context.bot.edit_message_text(
                text=query.message.text,
                chat_id=query.message.chat_id,
                message_id=query.message.message_id
            )
            # Elimina el mensaje
            query.message.delete()
            ret_data = True, datetime.date(year, month, day)
        case "PREV-MONTH":
            pre = curr - datetime.timedelta(days=1)
            re_create_calendar(pre)
        case "NEXT-MONTH":
            nex = curr + datetime.timedelta(days=31)
            re_create_calendar(nex)
        case _:
            context.bot.answer_callback_query(callback_query_id=query.id, text="¡Algo salió mal!")
            # UNKNOWN

    return ret_data
